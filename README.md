# Course: BPC2T - seminar 1
This project provides a solution for seminar 1. Includes topics like:
1. Class creation 
2. if/else constructs
3. switch-case
4. do-while / for loops
5. Distinguish between basic java data types
6. Declaring and working with multidimensional arrays

## Authors

email | Name 
------------ | -------------
pavelseda@email.cz | Šeda Pavel

## Starting up Project
Just import it in your selected IDE (Eclipse, IntelliJ IDEA, Netbeans, ...)

## Used Technologies
The project was built and tested with these technologies, so if you have any unexpected troubles let us know.

```
Maven         : 3.3.9
Java          : OpenJDK 11
```