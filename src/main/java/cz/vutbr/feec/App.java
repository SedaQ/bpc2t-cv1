package cz.vutbr.feec;

/**
 * 
 * @author Pavel Seda
 *
 */
public class App {

	public static void main(String[] args) {
		int age = 5;
		long id = 1L;
		String name = "Pavel Seda";
		if (age < 18) {
			System.out.println("This person is not allowed to drink beer.");
		} else {
			System.out.println("This person is allowed to drink beer.");
		}

		do {
			if (name.equals("Pavel Seda")) {
				break;
			}
		} while (true);

		// reimplementace vyse uvedeneho do-while cyklu do for cyklu
		for (;;) {
			if (name.equals("Pavel Seda")) {
				break;
			}
		}

		// switch case konstrukt na promennou name
		switch (name) {
		case "Pepa":
			System.out.println("Pepa");
		default:
			System.out.println("Please set the name to Pepa");
		}

		// matrix multiplication
		int[][] a = { { 1, 2, 3 }, { -2, 2, -1 } };
		int[][] b = { { 0, 1, 2 }, { -2, -1, -1 }, { 1, 0, 0 } };

		int[][] result = matrixMultiplicator(a, b);
		for (int i = 0; i < result.length; i++) {
			for (int j = 0; j < result[i].length; j++) {
				System.out.printf("%4d", result[i][j]);
			}
			System.out.println("");
		}

		// uložení většího typu do menšího (int do short)
		int i = 50;
		short myShort = (short) i;

	}

	public static int[][] matrixMultiplicator(int[][] a, int[][] b) {

		int aRows = a.length;
		int aColumns = a[0].length;
		int bRows = b.length;
		int bColumns = b[0].length;

		if (aColumns != bRows) {
			throw new IllegalArgumentException("A:Rows: " + aColumns + " did not match B:Columns " + bRows + ".");
		}

		int[][] c = new int[aRows][bColumns];
		for (int i = 0; i < aRows; i++) {
			for (int j = 0; j < bColumns; j++) {
				c[i][j] = 0;
			}
		}

		for (int i = 0; i < aRows; i++) { // aRow
			for (int j = 0; j < bColumns; j++) { // bColumn
				for (int k = 0; k < aColumns; k++) { // aColumn
					c[i][j] += a[i][k] * b[k][j];
				}
			}
		}

		return c;
	}
}
